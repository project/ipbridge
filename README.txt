/* $Id: */

This is a bridge module that allows Drupal to use IP.Board members table to authenticate users.

Installation

1 Upload the files
2 Tell Drupal about your IPB database

in settings.php you should have somethig like this:

/*
* 'default' is your main drupal database
* 'ipb' is your IPB database
*/

$databases = array (
  'default' =>
  array (
    'default' =>
    array (
      'driver' => 'mysql',
      'database' => 'main_database',
      'username' => 'drupal_database_user',
      'password' => 'drupal_database_pass',
      'host' => 'localhost',
      'port' => '',
      'prefix' => '',
    ),
    'ipb' =>
    array (
      'driver' => 'mysql',
      'database' => 'ipb_database',
      'username' => 'ipb_database_user',
      'password' => 'ipb_database_pass',
      'host' => 'localhost',
      'port' => '',
      'prefix' => '',
    ),
  ),
);

3. Activate the module